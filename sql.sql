-- DROP TABLE Imprimantes;
-- DROP TABLE Usagers;
-- DROP TABLE Reservations;

CREATE TABLE Imprimantes
	(
    NoImp INT NOT NULL PRIMARY KEY,
    NomImprimante VARCHAR(7) NOT NULL,
    Couleur VARCHAR(10),
    NumeroSerie VARCHAR(15)
    );
    
CREATE TABLE Usagers
  (
  NoUsager INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Login VARCHAR(50) NOT NULL,
  Mdp VARCHAR(8) NOT NULL,
  Statut ENUM('Professeur', 'Technicien', 'Etudiant') NOT NULL DEFAULT "Etudiant",
  DateCertificat DATE NULL DEFAULT "2016-12-25"
  );

-- INSERT INTO Usagers VALUES(NULL, "tests@polymtl.ca", "123", "Technicien", "2014-02-13");

CREATE TABLE Reservations
	(
    NoReservation INT PRIMARY KEY AUTO_INCREMENT,
    refUsager INT NOT NULL,
    refImprimante INT NOT NULL,
    Justification VARCHAR(20) NOT NULL,
    NoSemaine INT NOT NULL,
    NoBloc INT NOT NULL
    );
 
 -- INSERT INTO Reservations VALUES(NULL,1, 2,"Projet III - Tuyere",2,13);
    
    