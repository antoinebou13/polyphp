<?php
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$Login = $Mdp = $confirm_Mdp = "";
$Login_err = $Mdp_err = $confirm_Mdp_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate username
    if(empty(trim($_POST["Login"]))){
        $Login_err = "Please enter a username.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_Login);
            
            // Set parameters
            $param_Login = trim($_POST["Login"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $Login_err = "This username is already taken.";
                } else{
                    $Login = trim($_POST["Login"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Validate password
    if(empty(trim($_POST["Mdp"]))){
        $Mdp_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST["Mdp"])) < 6){
        $Mdp_err = "Password must have atleast 6 characters.";
    } else{
        $Mdp = trim($_POST["Mdp"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_Mdp"]))){
        $confirm_Mdp_err = "Please confirm password.";     
    } else{
        $confirm_Mdp = trim($_POST["confirm_Mdp"]);
        if(empty($Mdp_err) && ($Mdp != $confirm_Mdp)){
            $confirm_Mdp_err = "Password did not match.";
        }
    }
    
    // Check input errors before inserting in database
    if(empty($Login_err) && empty($Mdp_err) && empty($confirm_Mdp_err)){
        
        // Prepare an insert statement
        $sql = "INSERT INTO users (Login, Mdp) VALUES (?, ?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_Login, $param_Mdp);
            
            // Set parameters
            $param_Login = $Login;
            $param_Mdp = password_hash($Mdp, PASSWORD_DEFAULT); // Creates a password hash
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                header("location: login.php");
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
        <h2>Sign Up</h2>
        <p>Please fill this form to create an account.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($Login_err)) ? 'has-error' : ''; ?>">
                <label>Username</label>
                <input type="text" name="Login" class="form-control" value="<?php echo $Login; ?>">
                <span class="help-block"><?php echo $Login_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Password</label>
                <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
                <span class="help-block"><?php echo $confirm_password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
            <p>Already have an account? <a href="login.php">Login here</a>.</p>
        </form>
    </div>    
</body>
</html>